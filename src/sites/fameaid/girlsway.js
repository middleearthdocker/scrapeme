const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('girlsway','https://www.girlsway.com/en/videos//updates/0/Pornstar/1/0');
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.tlcItem').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.tlcSourceSite').find('a').text().trim();
      scene.date = this.date( info.find('.tlcSpecsDate > .tlcDetailsValue').text() );
      scene.type = 'scene';
      scene.title = info.find('.tlcTitle').find('a').attr('title');
      scene.url = 'https://girlsway.com'+info.find('.tlcTitle > a').attr('href');
      scene.source_id = scene.url.match(/\/(\d+)$/)[1];

      scene.actors = info.find('.tlcActors').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.tags = info.find('.tlcCategories').find('a').map( (i,tag)=>{
        return $(tag).attr('title');
      }).get();
      scene.images = info.children('a').find('img').map( (i,img)=>{
        return $(img).attr('src');
      }).get();
      scene.description = $('.tlcItemDescription').find('span').last().text();

      if( ! scene.url ) continue;

      if( await this.save(scene) ) break; 
    }
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.girlsway.com/en/videos//updates/0/Pornstar/${page_number+1}/0`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
