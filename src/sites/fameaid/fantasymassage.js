const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('fantasymassage','https://www.fantasymassage.com/en/videos',true);
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.tlcItem').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.tlcSourceSite').find('a').text().trim();
      scene.date = this.date( info.find('.tlcSpecsDate > .tlcDetailsValue').text() );
      scene.type = 'scene';

      scene.title = info.find('.tlcTitle').find('a').attr('title');
      scene.url = 'https://fantasymassage.com'+info.find('.tlcTitle > a').attr('href');
      scene.source_id = info.find('.tlcImageItem').attr('id').replace(/^\w+_/,'');

      scene.actors = info.find('.tlcActors').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.tags = info.find('.tlcCategories').find('a').map( (i,tag)=>{
        return $(tag).attr('title');
      }).get();
      scene.images = info.children('a').find('img').map( (i,img)=>{
        return $(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    scene.description = $('.sceneDesc').text().replace('Video Description:','').trim();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.fantasymassage.com/en/videos/AllCategories/0/Actor/0/updates/${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
