const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('milehighmedia','https://www.evilangel.com/en/videos');
    this.base_url = 'https://www.evilangel.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.tlcItem').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = 'evilangel';
      scene.date = this.date( info.find('.tlcSpecsDate > .tlcDetailsValue').text() );
      scene.type = 'dvd';

      scene.title = info.find('.tlcTitle').find('a').attr('title');
      scene.url = this.base_url + info.children('a').attr('href');
      scene.source_id = info.children('a').attr('id').replace('tlcItem_','');
      scene.actors = info.find('.tlcActors').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.dvd.title = $('.sourceDvdCaption > span').last().find('a')
                      .attr('title');
    scene.tags = $('.sceneColCategories').find('a').map( (i,tag)=>{
      return $(tag).attr('title');
    }).get();
    scene.description = $('.sceneDesc').text().replace('Description:','').trim();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.evilangel.com/en/videos/All/updates/${page_number+1}/0/Category/0`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
