const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('vivid','https://www.vivid.com/videos/Vivid_Videos.html');
    this.base_url = 'https://www.vivid.com/videos/Vivid_Videos.html';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.dvd-box-container').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.type = 'scene';
      scene.site = info.find('.badge.pull-right').text().trim();
      scene.date = this.date( 
        info.find('.released').text().replace('Released:','').trim(),
        'MMM DD, YYYY' 
      );
      scene.title = info.find('a').attr('title');
      scene.url = this.base_url + info.find('a').attr('href');
      scene.source_id = scene.url.match(/\/videos\/(\d+)\//)[1];
      
      scene.actors = info.find("span[ng-show='details.cast.length']").find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return $(img).attr('ng-source');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);
      await page.goBack({waitUntil: 'networkidle2'});

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.individual-scenes-section');

    scene.description = info.children('p').text().trim();
    scene.tags = info.children('h5').last().find('a').map( (i,tag)=>{
      return $(tag).text();
    }).get();
  }

  async nextPage(page,index,page_number){
    let elems = await page.$$("li[ng-if='directionLinks'] > a[backout-exclude=true]");
    await elems[1].click();
    await page.waitFor(1000);
  }
}

module.exports = scraper;
