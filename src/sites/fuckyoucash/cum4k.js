const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('fuckyoucash','https://cum4k.com/?page=1');
    this.baseurl = 'https://cum4k.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.card').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = 'cum4k';
      scene.type = 'scene';

      scene.title = info.find('.card-title').text();
      scene.date = this.date( info.find('.date').text(),'MMMM DD, YYYY' );
      scene.url = this.baseurl+info.children('a').attr('href');

      scene.actors = info.find('.actors').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return 'https:'+$(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      if( await this.save(scene) ) break;
    }
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://cum4k.com/?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
