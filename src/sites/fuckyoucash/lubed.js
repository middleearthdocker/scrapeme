const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    let host = 'lubed';
    super('fuckyoucash',`https://${host}.com/?page=1`);
    this.baseurl = `https://${host}.com`;
    this.basehost= host;
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.card').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = this.basehost;
      scene.type = 'scene';

      scene.title = info.find('.card-title').text();
      scene.date = this.date( info.find('.date').text(),'MMMM DD, YYYY' );
      scene.url = this.baseurl+info.children('a').attr('href');

      scene.actors = info.find('.actors').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return 'https:'+$(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
      await this.scrape_item_i( page, scene.url, scene );
 
      if( await this.save(scene) ) break;
    }
  }

  async scrape_item_i( page, url, scene ){
    await page.goto( url, {waitUntil: 'networkidle2'} );
    let $ = cheerio.load(await page.content());
    scene.actors = $('.details > div > div.col-6').last().find('a').toArray()
          .map( actor=>
             $(actor).text()
          );
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://${this.basehost}.com/?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
