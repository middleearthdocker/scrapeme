const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'xart',
      'https://www.x-art.com/videos/recent/Lesbian/'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.item').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = 'x-art';
      scene.title = info.find('.item-header').find('h1').text();
      scene.date = this.date(
        info.find('.item-header').find('h2').text(),
        'MMM DD, YYYY'
      );
      scene.url = info.parent().attr('href');
      scene.images = info.find('.item-img').find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;

      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.actors = $("span:contains('featuring')").parent().find('a').map( (i,actor)=>
      $(actor).text()
    ).get();
    scene.description = $("span:contains('featuring')").parent().parent()
                        .children('p').text().trim();
  }

  async nextPage(page,index,page_number){
    await page.goto(`https://www.x-art.com/videos/recent/${page_number+1}`)
  }
}

module.exports = scraper;
