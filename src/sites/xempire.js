const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    let hosts = ['darkx','hardx','lesbianx','eroticax'];
    let sites = hosts.map(host=>`https://www.${host}.com/en/videos/AllCategories/0/1`);
    super('xempire',sites);
    this.hosts = hosts;
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.sceneContainer').toArray();
    let host = this.hosts[index];
    let base = `https://${host}.com`;

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = host;
      scene.title = info.find('.sceneTitle').find('a').attr('title');
      scene.url = base + info.find('.sceneTitle').find('a').attr('href');
      scene.source_id = scene.url.match(/(\d+)$/)[1];
      scene.date = this.date(info.find('.sceneDate').text(),'MM-DD-YYYY');

      scene.actors = info.find('.sceneActors').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('.sceneDesc').text().replace('Video Description:','')
                        .trim();
    scene.tags = $('.sceneCol.sceneColCategories').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    let host = this.hosts[index];    
    await page.goto(
      `https://www.${host}.com/en/videos/AllCategories/0/${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
