const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('metart','https://www.metartnetwork.com/movies/',true);
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.custom-list-item-default').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.pull-right.custom-list-item-like').text().trim();
      scene.date = info.find('.custom-list-item-date').text()
                   .split('\t')[0]
      scene.date = this.date( scene.date, 'MMM D, YYYY' );
      scene.type = 'scene';
      scene.url = info.find('.custom-img-container').find('a').attr('href');

      let actor_urls = [];
      scene.actors = info.find('.custom-list-item-name').find('a').map( (i,actor)=>{
        actor_urls.push({
          actor: this.newActor($(actor).text()),
          url: $(actor).attr('href')
        });
        return $(actor).text();
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return $(img).attr('src').replace(/^https.*http/,'http');
      }).get();

      if( ! scene.url ) continue;

      for( let i=0; i<actor_urls.length; i++ ){
        await this.scrape_actor(actor_urls[i].actor,actor_urls[i].url,page);
      }

      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_actor(actor,url,page){
    await page.goto(url, {waitUntil: 'networkidle2'});
    let $ = cheerio.load(await page.content());
    let female_tags = ['pussy','nipples','breasts','labia'];
    let tags = $(".tag-container").find('.list-of-tags').find('a').toArray();
    
    for( let i=0; i<tags.length; i++ ){
      let tag = $(tags[i]).text();
      for( let j=0; j<female_tags.length; j++ ){
        if( tag.indexOf(female_tags[j]) > -1 ){
          actor.gender = 'female';
          await this.saveActor(actor);
          return;
        }
      }
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.panel-body'); 

    scene.title = info.find('.pull-left.custom-photo-movie-description')
                      .find('a').attr('title');
    scene.tags = info.find('.list-of-tags').find('a').map( (i,tag)=>{
      return $(tag).text();
    }).get();
    scene.description = info.find('.custom-description-long.custom-description-hide')
                            .find('p').text()
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.metartnetwork.com/movies/latest/${page_number+2}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
