const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('straplezz','https://straplezz.com/categories/movies.html');
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.card.thumbnail').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = 'straplessdildo';
      scene.type = 'scene';
      scene.date = info.find('.far.fa-calendar-alt').parent().text().trim();
      scene.date = this.date( scene.date, 'MMMM D, YYYY' );
      scene.url = info.children('a').attr('href');
      scene.title = info.find('.card-title').text();

      scene.actors = info.find('.tour_update_models').find('a').map( (i,actor)=>{
        return $(actor).text().trim();
      }).get();

      if( ! scene.url ) continue;

      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.panel-body'); 

    scene.tags = $('.tag').find('a').map( (i,tag)=>{
      return $(tag).text().trim();
    }).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://straplezz.com/categories/movies_${page_number+1}_d.html`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
