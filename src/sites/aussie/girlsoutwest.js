const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('girlsoutwest','https://tour.girlsoutwest.com/categories/movies/1/latest/');
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.latestScene').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.site = this.network;
      
      scene.type = 'scene';
      scene.title = info.find('h4').find('a').attr('title');
      scene.date = this.date(
          info.children('p').find('span').text().split('|')[1].trim()
      );
      scene.url =  info.find('h4').find('a').attr('href');

      scene.actors = info.children('p').children('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();

      if( ! scene.url ) continue;
      if( await this.save(scene) ) break;
    }
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://tour.girlsoutwest.com/categories/movies/${page_number+1}/latest/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
