const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'cherrypimps',
      'https://cherrypimps.com/categories/movies_1_d.html'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.video-thumb').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.site = this.network;
      scene.type = 'scene';

      scene.date = info.find('.date').text().split('|')[1].trim();
      scene.date = this.date( scene.date );
      scene.title = info.children('a').attr('title');

      scene.url = info.children('a').attr('href');
      if( scene.url === undefined || (!scene.url) ) continue;
      scene.source_id = scene.url.match(/trailers\/(\d+)\-/)[1];

      scene.actors = info.find('.category').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.info-block > .info-block_title').parent();

    scene.description = info.find('p.text').first().text().trim();
    scene.tags = info.find('.text').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://cherrypimps.com/categories/movies_${page_number+1}_d.html`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
