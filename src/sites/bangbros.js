const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'bangbros',
      'https://bangbros.com/videos'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.echThumb').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.date = this.date( 
        info.find('.thmb_mr_cmn.thmb_mr_2 > span').text()
        , 'MMM DD, YYYY'
      );
      scene.site = info.find('.thmb_mr_lnk').find('span').text().trim();
      scene.title = info.children('a').attr('title');
      scene.url = 'https://bangbros.com'+info.children('a').attr('href');
      scene.source_id = info.children('a').attr('id').replace('video-','');

      scene.actors = info.find('.cast-wrapper').find('a').map( (i,actor)=>{
        return $(actor).text().trim();
      }).get();
      scene.images = info.children('a').find('img').map( (i,img)=>
        'https:'+$(img).attr('data-src')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('.vdoDesc').text().trim();
    scene.tags = $('.vdoTags').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://bangbros.com/videos/${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
