const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'joymii',
      'https://joymii.com/site/videos?tab=latest&page=1'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.set').toArray();
    let baseurl = 'https://joymii.com';

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = 'joymii';
      scene.date = this.date( info.find('.box-release_date.release_date').text(), 'MMM DD, YYYY');
      scene.title = info.find('.box-title.title').find('a').text();
      scene.url = baseurl + info.children('a').attr('href');
      scene.source_id = scene.url.match(/\/(\d+)_\w+$/)[1];

      scene.actors = info.find('.box-actors.actors').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('.text').parent().find('p').text().trim();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://joymii.com/site/videos?tab=latest&page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
