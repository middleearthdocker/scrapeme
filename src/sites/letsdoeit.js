const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'letsdoeit',
      'https://letsdoeit.com/videos?sort=recent&page=1'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.video-item').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = info.find('.channel').text();
      scene.title = info.find('.title').text();
      scene.url = 'https://letsdoeit.com'+info.find('.title').attr('href');
      scene.source_id = info.find('a.item-top').attr('data-id');

      let actors = [];
      scene.actors = info.find('.left').children('a').map( (i,actor)=>{
        actors.push($(actor).attr('href'));
        return $(actor).text();
      }).get();

      if( ! scene.url ) continue;

      for( let i=0; i<actors.length; i++ ){
        await this.scrape_actor(
          this.newActor( scene.actors[i] ),
          'https://letsdoeit.com' + actors[i],
          page
        );
      }
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_actor(actor, url, page){
    await page.goto(url, {waitUntil: 'networkidle2'});
    let $ = cheerio.load(await page.content());
    let description = $('.pornstar-description').attr('data-text').trim();
    
    for( let i=0; i<this.pronouns.length; i++ ){
      let word = this.pronouns[i];
      if( description.indexOf(word.term) > -1 ){
        actor.gender = word.gender;
        return await this.saveActor(actor);
      }
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    
    scene.date = this.date( 
      $('.col.date').text().trim(),
      'DD.MM.YY'
    );

    if( ! scene.site ){
      scene.site = $('.big-container').find('.channel > a').text();
    }
    scene.description = $('.description').attr('data-text').trim();
    scene.tags = $('.card.card-body').find("a:not('.pornstar')").map( (i,tag)=>
      $(tag).attr('title')
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://letsdoeit.com/videos?sort=recent&page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }

  async beforeScrape(page,index){
    let $ = cheerio.load(await page.content());
    if( $('.video-item').length < 3 ){
      await page.click('.cta-enter');
      await page.waitFor('.video-item',{waitUntil:'networkidle2'});
    }
  }
}

module.exports = scraper;
