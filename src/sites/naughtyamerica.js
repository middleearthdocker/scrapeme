const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'naughtyamerica',
      'https://www.naughtyamerica.com/new-porn-videos'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.scene-item').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.date = this.date( info.find('.entry-date').text(), 'MMM DD, YYYY' );
      scene.site = info.find('.site-title').text().trim();
      scene.title = info.children('a').attr('title');
      scene.url = info.children('a').attr('href').replace(/\?nats.*$/,'');
      scene.source_id = scene.url.match(/\-(\d+)$/)[1];

      scene.actors = info.find('.contain-actors').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.children('a').find('img').map( (i,img)=>
        'https:'+$(img).attr('src')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('.synopsis').text().replace('Synopsis','').trim();
    scene.tags = $('.categories').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.naughtyamerica.com/new-porn-videos?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
