const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'teenmegaworld',
      'https://teenmegaworld.net/categories/movies.html'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('article').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = info.find('aside').children('div').last().find('a').text()
                   .replace(/\.\w+$/,'').trim();
      scene.date = this.date( info.find('time').text(), 'MMMM DD, YYYY' );
      scene.title = info.find('h1 > a').text();
      scene.url = info.find('h1 > a').attr('href');

      scene.actors = info.find('.actors').find('a').map( (i,actor)=>{
        return $(actor).attr('data-name');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('footer > .description').text().trim();
    scene.tags = $('.tags').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://teenmegaworld.net/categories/movies_${page_number+1}_d.html`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
