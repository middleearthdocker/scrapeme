const cheerio = require('cheerio'),
  Scraper = require('../..//scraper.js');

class scraper extends Scraper {
  constructor(){
    super('radicalcash','https://tour.purgatoryx.com/episodes?page=1');
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.content-item').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = 'purgatoryx';
      scene.type = 'scene';
      scene.date = info.find('.pub-date').text().trim();
      scene.date = this.date( scene.date, 'MMM DD, YYYY' );
      scene.url = info.find('.title').find('a').attr('href');
      scene.title = info.find('.title').text().trim();

      scene.actors = info.find('.models').find('a').map( (i,actor)=>{
        return $(actor).text().trim();
      }).get();
      scene.images = info.find('.row.thumbnails-wrap').find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;

      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.description = $('.description').find('p').text().trim();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://tour.purgatoryx.com/episodes?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
