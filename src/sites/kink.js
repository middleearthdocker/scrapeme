const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'kink',
      'https://www.kink.com/shoots/latest'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.shoot').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.date = this.date( info.find('.date').text(), 'MMM DD, YYYY');
      scene.title = info.find('.shoot-thumb-title').find('a').text();
      scene.url = 'https://kink.com'+info.find('.shoot-thumb-title').find('a')
                  .attr('href');
      scene.source_id = scene.url.match(/\/(\d+)$/)[1];

      scene.actors = info.find('.shoot-thumb-models').find('a').map( (i,actor)=>{
        return $(actor).text().trim();
      }).get();
      scene.images = info.find('.shoot-thumb-image').find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.site = $('.column.shoot-logo').find('a').attr('href').replace('/channel/','');
    scene.description = $('.description').text().trim();
    scene.tags = $('.tag-list.category-tag-list').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.kink.com/shoots/latest?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
