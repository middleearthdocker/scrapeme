const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'teamskeet',
      'https://www.teamskeet.com/t1/updates/load?view=newest&fltrs[tags]=&fltrs[site]=ALL&page=1&changedOrder=0&fltrs[tags]=&fltrs[time]=ALL&fltrs[site]=ALL&order=DESC&tags_select=&fltrs[title]='
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('#updatesList > .white').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = info.find('.info').children('div').last().find('a').text().trim();
      scene.url = info.children('a').attr('href');
      scene.source_id = info.children('a').attr('id');
      scene.title = scene.url.match(/view\/([\w_]+)\//)[1].replace(/_/g,' ');
      scene.date = this.date( 
        info.find('.info').find('p').first().text()
        ,'MM/DD/YYYY'
      );

      scene.images = info.children('a').find('img').map( (i,img)=>
        $(img).attr('data-original')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    scene.description = $('.gray').last().text().trim();

    let actors = $('h3').find('a').attr('href')
                 .match(/\/\d+\/([\w_]+)\//)[1].replace(/_/g,' ');
    scene.actors = actors.split(/\s*and\s*/);

    scene.tags = $('div:contains("Tags:")').parent().children('div').last().find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.teamskeet.com/t1/updates/load?view=newest&fltrs[tags]=&fltrs[site]=ALL&page=${page_number+1}&changedOrder=0&fltrs[tags]=&fltrs[time]=ALL&fltrs[site]=ALL&order=DESC&tags_select=&fltrs[title]=`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
