const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'private',
      'https://www.private.com/scenes/'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.scene').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.date = this.date( info.find('.scene-date').text(), 'MM/DD/YYYY' );
      scene.title = info.children('a').attr('title');
      scene.url = info.children('a').attr('href');
      scene.source_id = scene.url.match(/\/(\d+)$/)[1];

      scene.actors = info.find('.scene-models').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.find('picture').find('source').map( (i,img)=>
        $(img).attr('srcset').trim().replace(/\s\d+w$/,'')
      ).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());

    scene.site = $('.logos-sites').find('.title-site').text().replace(/\.\w+$/,'').trim();
    scene.description = $('#description-section').text().trim();
    scene.tags = $('.scene-tags').find('a').map( (i,tag)=>
      $(tag).text()
    ).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.private.com/scenes/${page_number+1}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
