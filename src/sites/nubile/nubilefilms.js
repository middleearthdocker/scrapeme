const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('nubilefilms','https://nubilefilms.com/video/gallery/');
    this.base_url = 'https://nubilefilms.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.videoset').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.site = this.network;
      
      scene.type = 'scene';
      scene.title = info.find('.title').text();
      scene.date = this.date( info.find('.date').text(), 'MMM DD, YYYY' );
      scene.url = this.base_url + info.find('.title').attr('href');
      scene.source_id = scene.url.match(/watch\/(\d+)\//g)[0].replace(/\D+/g,'');

      let actor_urls = [];
      scene.actors = info.find('.models').find('.model').map( (i,actor)=>{
        actor_urls.push({
          actor: this.newActor( $(actor).text() ),
          url: this.base_url + $(actor).attr('href')
        });
        return $(actor).text();
      }).get();
      scene.images = info.children('.inner-wrapper').find('img').map( (i,img)=>{
        return 'https:'+ $(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
      
      for( let i=0; i<actor_urls.length; i++ ){
        await this.scrape_actor(actor_urls[i].actor,actor_urls[i].url,page);
      }
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);
      await page.goBack({waitUntil: 'networkidle2'});

      if( await this.save(scene) ) break;
    }
  }

  async scrape_actor(actor,url,page){
    await page.goto(url, {waitUntil: 'networkidle2'});
    let $ = cheerio.load(await page.content());
    let male = [' he ', ' him '];    

    if( $.text().match(/Figure\s+\w+\-\w+/g) ){
      actor.gender = 'female';
    }
    else{
      let description = $('.model-bio').text().trim();
      for( let i=0; i<male.length; i++ ){
        if( description.indexOf(male[i]) > -1 ){
          actor.gender = 'male';
          break;
        }
      }
    }

    await this.saveActor(actor);
    await page.goBack({waitUntil: 'networkidle2'});
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.descrips');

    scene.tags = info.find('.tags').find('a.wptag').map( (i,tag)=>{
      return $(tag).text()
    }).get();
    scene.description = info.find('.video-description').find('p').map( (i,p)=>
      $(p).text()
    ).get().join('');
  }

  async nextPage(page,index,page_number){
    await page.click('a.next');
    await page.waitFor('.videoset',{waitUntil: 'networkidle2'});
  }
}

module.exports = scraper;
