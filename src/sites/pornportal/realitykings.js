const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('realitykings','https://www.realitykings.com/tour/videos/all-sites/all-categories/all-time/recent/');
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.card--release').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.card-info__meta').find('a').attr('title').trim();
      scene.date = this.date(info.find('.card-info__meta-date').text().trim(),'MMMM DD, YYYY');
      scene.type = 'scene';

      scene.title = info.find('.card-info__title').find('a').attr('title');
      scene.url = 'https://realitykings.com'+info.find('.card-info__title > a').attr('href');
      scene.source_id = scene.url.match(/\/watch\/(\d+)\//)[1];

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    
    scene.description = $('.read-container').find('p').text().trim();
   
    scene.actors = $('#trailer-desc-txt').find('.models-name > a').map( (i,actor)=>{
      return $(actor).attr('title');
    }).get();

  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.realitykings.com/tour/videos/all-sites/all-categories/all-time/recent/${page_number+1}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
