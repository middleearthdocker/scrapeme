const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'brazzers',
      'https://www.brazzers.com/pornstars/all-pornstars/female/all-categories/any/recent/'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.model-card-title').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]).find('a');
      if( info.attr('title').indexOf('...') > -1 ) continue;
      let actor = this.newActor(info.attr('title'));
      actor.gender = 'female';
      if(await this.saveActor(actor)){
        return;
      }
    }
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.brazzers.com/pornstars/all-pornstars/female/all-categories/any/recent/${page_number+1}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
