const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('brazzers','https://www.brazzers.com/videos/',true);
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.release-card.scene').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.release-information').find('a').attr('title').trim();
      scene.date = this.date( info.find('time').text().trim(),'MMMM DD, YYYY' );
      scene.type = 'scene';

      scene.title = info.find('.scene-card-title').find('a').attr('title');
      scene.url = 'https://brazzers.com'+info.find('.scene-card-title > a').attr('href');
      scene.source_id = scene.url.match(/\/id\/(\d+)\//)[1];

      scene.actors = info.find('.model-names').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('.card-image').find('img').map( (i,img)=>{
        return 'https:'+$(img).attr('data-src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    scene.description = $('.video-description').text()
                          .replace('Collapse','').trim();
    scene.tags = 
      $('.tag-card-container').find('a').map( (i,tag)=>{
        return $(tag).attr('title');
      }).get()
      .concat(
        $('.timeline').find('a').map( (i,tag)=>$(tag).attr('title') ).get()
      );
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.brazzers.com/videos/all-sites/all-pornstars/all-categories/alltime/bydate/${page_number+1}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
