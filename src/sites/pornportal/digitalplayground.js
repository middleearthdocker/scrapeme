const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('digitalplayground','https://www.digitalplayground.com/scenes');
    this.base = 'https://digitalplayground.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.ajpcz4-1 > .dtkdna-1').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = this.network;
      scene.type = 'dvd';
      scene.date = info.find('.dtkdna-3 > .dtkdna-5').first().text();
      scene.date = this.date( scene.date,'MMM DD, YYYY' );

      scene.title = info.find('.aq1tgu-0').find('a').attr('title').trim();
      scene.url = this.base+info.find('.aq1tgu-0').find('a').attr('href');
      
      let title_split = scene.title.split(/\s+/);
      scene.source_id = scene.url.match(/\/scene\/(\d+)\//)[1] +
                        title_split[title_split.length-2] +
                        title_split[title_split.length-1];
      

      if( scene.title.match(/(\d+)$/) ){
        scene.dvd.scene_number = parseInt(scene.title.match(/(\d+)$/)[1]);
      }

      scene.actors = info.find('span.i4pbj6-1 > .dtkdna-5').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return $(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);
      await page.goBack();

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.tjb798-1').children('div');

    scene.dvd.title = $('h1.wxt7nk-4').text().replace('Trailer','');
    scene.description = $('.tjb798-3:contains("Description")')
                        .parent().children('div').last().text();

    scene.tags = info.find('a').map( (i,tag)=>{
      return $(tag).text().trim();
    }).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.digitalplayground.com/scenes?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
