const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'sexyhub',
      'https://www.fitnessrooms.com/tour/videos/'
    );
    this.base = 'https://fitnessrooms.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.release-card.scene').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]).children('div');
      let scene = this.newScene();
      
      scene.site = 'fitnessrooms';
      scene.date = this.date(info.find('.release-date').text().trim(),'MMM< DD, YYYY');
      scene.type = 'scene';

      scene.title = info.find('.card-title > a').attr('title');
      scene.url = this.base + info.children('a').attr('href');
      scene.source_id = scene.url.match(/video\/(\d+)\//)[1];

      scene.actors = info.find('.model-name').find('a').map( (i,actor)=>{
        return $(actor).text();
      }).get();
      scene.images = info.children('a').find('img').map( (i,img)=>{
        return 'https:'+$(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    scene.description = $('.overview').text().trim();
    scene.tags = $('.categories').find('a').map( (i,tag)=>
      $(tag).attr('title')
    ).get();
    scene.tags = scene.tags.concat(
      $('div.col-tags > .paper-tiles').find('a').map( (i,tag)=>
        $(tag).text().trim()
      ).get()
    );
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.fitnessrooms.com/tour/videos/${page_number+1}/`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
