const cheerio = require('cheerio'),
  Scraper = require('../../scraper.js');

class scraper extends Scraper {
  constructor(){
    super('twistys','https://www.twistys.com/scenes');
    this.base = 'https://twistys.com';
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.ajpcz4-1 > .dtkdna-1').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      
      scene.site = info.find('.dtkdna-2 > .dtkdna-3').last().find('a')
                   .attr('title').trim();
      scene.type = 'scene';
      scene.title = info.find('.aq1tgu-0').find('a').attr('title');
      scene.date = info.find('.dtkdna-3 > .dtkdna-5').first().text();
      scene.date = this.date( scene.date,'MMM DD, YYYY' );
      scene.url = this.base+info.find('.aq1tgu-0').find('a').attr('href');
      scene.source_id = scene.url.match(/\/scene\/(\d+)\//)[1];

      scene.actors = info.find('span.i4pbj6-1 > .dtkdna-5').find('a').map( (i,actor)=>{
        return $(actor).attr('title');
      }).get();
      scene.images = info.find('img').map( (i,img)=>{
        return $(img).attr('src');
      }).get();

      if( ! scene.url ) continue;
 
      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    let info = $('.tjb798-1').children('div');

    scene.description = $('.tjb798-3:contains("Description")')
                        .parent().children('div').last().text();

    scene.tags = info.find('a').map( (i,tag)=>{
      return $(tag).text().replace(',','').trim();
    }).get();
  }

  async nextPage(page,index,page_number){
    await page.goto(
      `https://www.twistys.com/scenes?page=${page_number+1}`,
      {waitUntil: 'networkidle2'}
    );
  }
}

module.exports = scraper;
