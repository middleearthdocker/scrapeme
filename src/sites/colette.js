const cheerio = require('cheerio'),
  Scraper = require('../scraper.js');

class scraper extends Scraper {
  constructor(){
    super(
      'colette',
      'https://www.colette.com/index.php?show=videos&sort=recent'
    );
  }

  async scrape_page(page, index){
    let $ = cheerio.load(await page.content());
    let containers = $('.item').toArray();

    for( let i=0; i<containers.length; i++ ){
      let info = $(containers[i]);
      let scene = this.newScene();
      scene.type = 'scene';

      scene.site = this.network;
      scene.title = info.find('.header.row').find('h1').text();
      scene.url = info.parent().attr('href');
      scene.description = info.find('.description').text().replace(/HD\svideo:?/i,'')
                          .replace(/\s{2,}/g,'');
      scene.images = info.find('.cover').find('img').map( (i,img)=>
        $(img).attr('src')
      ).get();

      if( ! scene.url ) continue;

      await page.goto(scene.url, {waitUntil: 'networkidle2'});
      await this.scrape_scene(page, scene);
      await page.goBack({waitUntil: 'networkidle2'});

      if( await this.save(scene) ) break;
    }
  }

  async scrape_scene(page, scene){
    let $ = cheerio.load(await page.content());
    
    scene.date = this.date( 
      $('.columns.info').find('h2').first().text(),
      'MMM DD, YYYY'
    );

    scene.actors = $('.columns.info').find('h2').find('a').map( (i,actor)=>
      $(actor).text()
    ).get();
  }

  async beforeScrape(page,index){
    let $ = cheerio.load(await page.content());
    if( $('a > .item').length < 3 ){
      await page.click('.btn-warning');
      await page.waitFor('a > .item',{waitUntil:'networkidle2'});
    }
    await page.goto(this.start_urls[index], {waitUntil:'networkidle0'});
    
    await page.evaluate(async () => {
      await new Promise((resolve, reject) => {
        let totalHeight = 0
        let distance = 100
        let timer = setInterval(() => {
          let scrollHeight = document.body.scrollHeight
          window.scrollBy(0, distance)
          totalHeight += distance
          if(totalHeight >= scrollHeight){
            clearInterval(timer)
            resolve()
          }
        }, 100)
      })
    })
    //await page.click('ul.middle-menu > li:nth-child(2) > a');
    //await page.waitFor('a > .item',{waitUntil:'networkidle2'});
  }
}

module.exports = scraper;
