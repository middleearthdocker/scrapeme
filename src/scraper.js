const moment = require('moment'),
  Save = require('./lib/save.js');

class Scraper {

  constructor(network, start_urls){
    this.network = network;
    this.start_urls = (start_urls.constructor === Array)? start_urls : [start_urls];
    
    this.stats = {
      error_messages: [],
      same: 0,
      errors: 0,
      duplicates: 0,
      additions: 0,
    };

    this.pronouns = [
      { term: ' he '  , gender: 'male' },
      { term: ' his ' , gender: 'male' },
      { term: ' him ' , gender: 'male' },
      { term: ' she ' , gender: 'female' },
      { term: ' hers ', gender: 'female' },
      { term: ' her ' , gender: 'female' }
    ];
  }

  /**
   * Methods to be overriden
   *
   * scrape_page - method to scrape page called after nextPage
   * nextPage - called to direct to next page to scrape
   * beforeScrape - called after navigating to start url
   *
   * @param {Page} page - Page object (puppeteer, or request module)
   * @param {number} index - index in start_urls
   * @param {page_number} page number - current page number
   */
  scrape_page(page, index){
    return new Promise((resolve)=>resolve(true));
  }

  nextPage(page, index, page_number){
    return new Promise((resolve)=>resolve(true));
  }

  beforeScrape(page,index){
    return new Promise((resolve)=>resolve(true));
  }

  /**
   * Start scrape method
   * @param {number} number_pages - the number of pages to scrape
   * @param {number} start_page - the page to start scraping
   * @param {string} url - the url of page to start on
   */
  startScrape(browser, number_pages = 3, start_page = 1, url ) {
    return browser.getPage()
    .then( page=>{

      if( url ){
        this.start_urls = [url];
      }

      return this.scrapeURLS(page,[...this.start_urls], number_pages, start_page)
      .then(()=>page.close());
    })
    .then(()=>this.stats);
  }

  scrapeSingleURL(page, current_page, end_page, index){
    if( current_page >= end_page || this.stats.same >=5 ){
      return new Promise(resolve=>resolve(true));
    }

    return this.scrape_page(page, index)
    .catch(e=>{
      this.stats.error_messages.push(
        `${this.network}: Error scraping page ${current_page} on index ${index}: ${e.message}`
      );
    })
    .then(()=>{
      return this.nextPage(page, index, current_page)
      .then(()=>this.scrapeSingleURL(page, current_page+1,end_page, index ))
      .catch(e=>{
        this.stats.error_messages.push(
          `${this.network}: Error with nextPage method: ${e.message}`
        );
      });
    })
  }

  scrapeURLS(page, urls, number_pages, start_page){
    if( urls.length === 0 ){
      return new Promise(resolve=>resolve(true));
    }

    // goto page method
    let url = urls.shift();
    let index = this.start_urls.indexOf(url);
    let end_page = start_page + number_pages;

    return page.goto( url, {waitUntil: 'networkidle2' } )
    .then(()=>{
      return this.beforeScrape(page,index)
      .then(()=>this.scrapeSingleURL(page, start_page, end_page, index))
      .catch(e=>{
        console.error(`${this.network}: Error with beforeScrape method`);
        console.error(e);
      });
    })
    .catch(e=>{
      console.error(`${this.network}: Error going to ${url}`);
      console.error(e);
    })
    .then(()=>this.scrapeURLS(page,urls,number_pages,start_page));
  }

  /**
   * Converts given date to Javascript Date
   * @param {string} date  - date to be formatted
   * @param {string} format - input date format (default YYYY-MM-DD)
   * @returns {Date} - Javascript Date object
   */
  date(date, format) {
     if( format ){
       return moment(date,format).utc().toDate();
     }
     else {
       return moment(date,'YYYY-MM-DD').utc().toDate();
     }
  }

  saveActor(actor){
    let stats = this.stats;
    let handleResponse = this.handleResponse;
    return Save(actor,'actor')
    .then( response=>
      handleResponse( stats, response, {
        addition:  (status)=> status === 1 || status === 2 || status === 4,
        duplicate: (status)=> status === false,
      })
    );
  }

  save(scene){
    let stats = this.stats;
    let handleResponse = this.handleResponse;
    return Save(scene,'scene')
    .then( response=>
      handleResponse( stats, response, {
        addition:  (status) => status === 1,
        duplicate: (status) => status === 2,
      })
    );
  }

  handleResponse(stats, response, conditions){
    if( conditions.duplicate(response.status) ){
      stats.duplicates++;
      stats.same++;
      return stats.same >=5;
    }
    else if( conditions.addition(response.status) ){
      stats.additions++;
      stats.same = 0;
    }
    else if(response.status === 3){
      stats.errors++;
      stats.same = 0;
      
      let error = response.message.message;
      if( (!error) || (typeof error === 'undefined' ) ){
        error = response.message;
      }

      if( stats.error_messages.indexOf(error) === -1 ){
        stats.error_messages.push(error);
      }
    }
    else{
      let error = `${response.status} not supported`;
      if( stats.error_messages.indexOf(error) === -1 ){
        stats.error_messages.push(error);
      }
    }

    return false;
  }

  newActor(name){
    return {
      name: name,
      gender: null,
      alias: []
    }
  }

  newScene(){
    return {
      network: this.network,
      site: null,
      date: null,
      type: null,
      dvd: {
        title: null,
        series: null,
        volume: null,
        volume_title: null,
        scene_number: null,
        frontCover: null,
        backCover: null,
      },
      title: null,
      actors: [],
      tags: [],
      url: null,
      images: [],
      source_id : null,
      description: null,
    }
  }
}

module.exports = Scraper;
