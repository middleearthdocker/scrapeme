'use strict';
const puppeteer = require('puppeteer');

const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0';
const skippedResources = [
  'quantserve',
  'adzerk',
  'doubleclick',
  'adition',
  'exelator',
  'sharethrough',
  'cdn.api.twitter',
  'google-analytics',
  'googletagmanager',
  'google',
  'fontawesome',
  'facebook',
  'analytics',
  'optimizely',
  'clicktale',
  'mixpanel',
  'zedo',
  'clicksor',
  'tiqcdn',
];
const blockedResourceTypes = [
  'image',
  'media',
  'font',
  'texttrack',
  'object',
  'beacon',
  'csp_report',
  'imageset',
];

class Browser {
  constuctor(){}

  async start(){
    let args = ['--no-sandbox','--disable-setuid-sandbox'];
    this.browser = await puppeteer.launch({
      headless: true,
      args: args
    });

    // connect to proxy

    return true;
  }

  close(){
    return this.browser.close();
  }

  async getPage(){
    let page = await this.browser.newPage();
    await page.setUserAgent(userAgent);
    await this.before_page(page);
    await page.setRequestInterception(true);
    await page.setViewport({ width: 1920, height: 1080 });

    page.on('request', request => {
      let requestUrl = request._url.split('?')[0].split('#')[0];
      if (
        blockedResourceTypes.indexOf(request.resourceType()) !== -1 ||
        skippedResources.some(resource => requestUrl.indexOf(resource) !== -1)
      ) {
        request.abort();
      } else {
        request.continue();
      }
    });
    
    return page;
  }

  before_page(page){
    return new Promise(resolve=>resolve(true));
  }
}

module.exports = Browser;
