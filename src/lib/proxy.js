'use strict';
const request = require('request');

module.exports = function(use_proxy){
  return new Promise( (resolve,reject)=>{
    if( ! use_proxy ) return resolve(false);

    let options = [
      'supportsHttps=true',
      'protocol=socks5'
    ];
    if( process.env['PROXY_KEY'] ){
      options.push('api_key='+process.env['PROXY_KEY']);
    }
    options = options.join('&');

    request(
      'https://gimmeproxy.com/api/getProxy?'+options
      ,function(err,res,body){
        if( err ){
          return reject(err);
        }

        body = JSON.parse(body);
        if( body.status_code === 429 ){
          resolve(false);
        }
        else{
          resolve(body.curl);
        }
      }
    );
  });
};