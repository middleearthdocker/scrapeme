const axios = require('axios')
  , path = require('path');

module.exports = function(object, type){
  if( ! type ){
    throw 'Type not specified';
  }

  object = cleanObject(object);
  if(! process.env['API_URL'] ){
    console.error(object);
    return new Promise(resolve=>resolve({status:1}));
  }
  let data = { API_KEY: process.env['API_KEY'] };
  data[type] = object;

  return axios.post( `${process.env['API_URL']}${type}/add`, data )
  .then( res=>{
    return res.data;
  })
  .catch( e=>{
    console.error(e.message);
    process.exit(1);
  });
}

function cleanObject(object){
  if( object === null || typeof object == 'undefined' ){
    return false;
  }
  else if( typeof object === 'string' ){
    return object.trim();
  }
  else if( object.constructor === Array ){
    let newArray = [];
    for( let i=0; i<object.length; i++ ){
      let value = cleanObject(object[i]);
      if( value ){
        newArray.push(value);
      }
    }
    return newArray;
  }
  else if( typeof object === 'object' ){
    let keys = Object.keys(object);
    let newObject = {};
    for( let i=0; i<keys.length; i++ ){
      if( keys[i] === 'date' ){
        newObject[keys[i]] = object[keys[i]];
        continue;
      }
      
      let value = cleanObject(object[keys[i]]);
      if( value ){
        newObject[keys[i]] = value;
      }
    }
    return newObject;
  }

  return object;
}
