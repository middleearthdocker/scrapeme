'use strict';
module.exports = function(jobs, results=[]){
  return syncJobs(jobs, results);
}

function syncJobs(jobs, results){
  if(jobs.length <= 0 ){
    return new Promise(resolve=>resolve(results));
  }

  let job = jobs.shift();
  return job
  .then( result=>results.concat(result))
  .then( res=>syncJobs(jobs,res));
}