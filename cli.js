#!/usr/bin/env node
'use strict'
const fs = require('fs')
  , path = require('path')
  , Table = require('cli-table')
  , ArgumentParser = require('argparse').ArgumentParser
  , Browser = require('./src/lib/browser.js');

let table = new Table({head: ['Errors','Duplicates','Additions']});
let parser = new ArgumentParser({addHelp: true,});

// parse input arguments
parser.addArgument('inputFile',{help: 'path to scraper'});
parser.addArgument(['-n'],{type:'int',defaultValue:3,help:'Number of pages'});
parser.addArgument(['-s'],{type:'int',defaultValue:1,help:'Start page number'});
parser.addArgument(['-j','--json'],{help:'Print output as json',action:'storeTrue'});
parser.addArgument(['--url','-u'],{help:'URL to parse using scraper'});
let args = parser.parseArgs();

// perform input error checks
if( args.n < 1 || args.s < 1 ){
  console.error('Number of pages must be a positive integer!');
  process.exit(1);
}
if( ! fs.existsSync(args.inputFile) ){
  console.error(`${args.inputFile} does not exist!`);
  process.exit(1);
}

// try to load scraper
try {
  let file = require(path.resolve(args.inputFile));
  let scraper = new file();
  let browser = new Browser();
  browser.start()
  .then( ()=>scraper.startScrape(browser,args.n, args.s, args.url) )
  .then( stats=>{
    if(!args.json){
      table.push([stats.errors, stats.duplicates, stats.additions]);
      console.log(table.toString());
      console.log('\n\nErrors');
      stats.error_messages.map( error=>console.log('\t\t',error) );
    }
    else{
      console.log(JSON.stringify(stats));
    }
  })
  .then(()=>browser.close());
}
catch(e){
  console.error(`Site file "${args.inputFile}" is not a valid scraper!`);
  console.error(e.message);
  process.exit(1);
};
