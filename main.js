#!/usr/bin/env node
'use strict';

const fs = require('fs'),
  path = require('path'),
  exec = require('child_process').exec,
  ArgumentParser = require('argparse').ArgumentParser,
  Table = require('cli-table');

let parser = new ArgumentParser({addHelp:true});
let table  = new Table({head:['Errors','Duplicates','Additions','Site']});
let command = ['node','cli.js','--json'];

// parse input arguments
parser.addArgument(['--test'],{
  defaultValue: false,
  dest: 'debug',
  action: 'storeTrue',
});
parser.addArgument(['-p','--parallel'],{
  help: 'Number of jobs to run in parallel',
  type: 'int',
  defaultValue: 4,
  dest: 'parallel',
});
let args = parser.parseArgs();

let tasks = [];
let errors = [];
let quit = [];
(async()=>{
  let test_sites = await getTestSites();
  let sites = (test_sites.length === 0)? walk('src/sites') : test_sites;
  let n = (args.parallel > sites.length)? sites.length : args.parallel;

  if(args.debug || test_sites.length >= 1){
    command = command.concat( '-n1' );
  }
  else{
    command = command.concat( '-n3' );
  }

  for( let site of sites ){
    tasks.push({
      site: path.basename(site),
      command: [...command,site].join(' '),
    });
  }
  
  let jobs = [];
  for( let i=0; i<n; i++ ){
    jobs.push(spawn());
  }
 
  return Promise.all(jobs)
  .then(()=>{
    console.log(table.toString());

    if( errors.length > 0 ){
      console.log('\n\nErrors'); 
      for( let error of errors ){
        if( error.messages.length > 0 ){
          console.log(`\t${error.site}`);
          error.messages.map(msg=>console.log(`\t\t${msg}`));
          
          if( args.debug && error.messages.length > 2 ){
            quit.push(`${error.site} has too many errors`);
          }
        }
      }
    }

    if( args.debug && quit.length > 0 ){
      console.error('\nFailed');
      quit.map(item=>console.error(`\t${item}`));
      process.exit(1);
    }
  });
})();

function spawn(){
  if( tasks.length === 0 ) return;
  let task = tasks.shift();
  return runCMD(task.command)
  .then( result=>processResults(JSON.parse(result),task.site) )
  .catch( e=>handleError(e,task.site) );    
}

function processResults(stats,site){
  let error = findObj(site,errors);
  if(! stats ){
    return spawn();
  }
  if( stats.errors != 0 ){
    error.messages = error.messages.concat(stats.error_messages);
  }

  table.push([stats.errors, stats.duplicates, stats.additions, site]);
  let total = stats.errors+stats.additions+stats.duplicates;
  let percent = stats.additions *1.0/total * 100.0;

  if( total === 0 ){
    let msg = `${site} is not parsing anything!`;
    if( args.debug ){
      quit.push(msg);
    }
    else {
      error.messages.push(msg);
    }
  }
  
  if( percent < 60 ){
    let msg = `${site} has too few additions`;
    if( args.debug ){
      quit.push(msg);
    }
    else{
      error.messages.push(msg);
    }
  }
  return spawn();
}

function handleError(e,site){
  let error = findObj(site,errors);
  if(e.message){
    error.messages.push(e.message);
  }
  else{
    error.messages.push(e);
  }
  return spawn();
}

function findObj(site, list, def={messages:[]}){
  let obj = list.filter(item=>item.site === site);
  if( obj.length === 0 ){
    list.push({site:site, ...def});
    return findObj(site,list,def);
  }
  return obj[0];
}

async function getTestSites(){
  let sites = [];
  let ignore = ['package-lock.json','main.js','gitlab-ci.yml','.gitlab-ci.yml'];
  if( args.debug ){
    let files = (await runCMD('git diff --name-status origin/master')).trim().split('\n');
    for( let file of files ){
      file = file.replace(/^\w+\s+/,'');
      
      if( file.indexOf("src/sites/") > -1 ){
        if( file.endsWith('.js') ){
          sites.push(file);
        }
      }
      else if( ignore.indexOf(file) === -1 ){
        return [];  
      }
    }
  }
  return sites;
}
function runCMD(command){
  return new Promise( (resolve,reject)=>{
    exec(command, (err,stdout,stderr)=>{
      if(err){
        reject(err);
      }
      else if(stderr){
        reject(stderr);
      }
      else{
        resolve(stdout);
      }
    });
  });
}
function walk(dir) {
  var results = [];
  var list = fs.readdirSync(dir);
  list.forEach(function(file) {
    file = dir + '/' + file;
    var stat = fs.statSync(file);
    if (stat && stat.isDirectory()) { 
      results = results.concat(walk(file));
    } else if(file.endsWith('.js')) { 
      results.push(file);
    }
  });
  return results;
}
